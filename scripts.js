//TODO:
//
//operations
//- default writeElt when no rootObj
//- save username
//- save color
//- notes counter
//- multi board
//- board name
//
//item
//- close button
//- go to parent button
//- remove button
//- edit button
//- save x,y position
//- images support
//- draggable with zoom 


// OPERATIONS 

function createAddElt(e) {

    if (document.body.contains(addElt)) addElt.parentNode.removeChild(addElt); 
    var obj = this;
    var selection = window.getSelection();
    console.log(selection);

    if (selection.anchorNode.parentNode.parentNode == selection.focusNode.parentNode.parentNode) {
        if ((selection.anchorOffset == selection.focusOffset) && (selection.anchorNode.parentNode == selection.focusNode.parentNode)) {
        } else {
            clientX = window.pageXOffset + e.clientX;
            clientY = window.pageYOffset + e.clientY;
            addElt = document.createElement('input');
            addElt.id = 'add';
            addElt.type = 'button';
            addElt.value = '+';
            addElt.style.left = window.pageXOffset + e.clientX + 0 + 'px';
            addElt.style.top = window.pageYOffset + e.clientY + 0 + 'px';
            addElt.addEventListener('click', createWriteElt.bind(obj), false);
            document.body.appendChild(addElt);
        }
    }

}



function createWriteElt(e) {

    if (document.body.contains(addElt)) addElt.parentNode.removeChild(addElt); 
    if (document.body.contains(writeElt)) writeElt.parentNode.removeChild(writeElt); 

    var obj = this;
    var selection = window.getSelection();
    console.log(selection);

    if (selection.anchorNode.parentNode.parentNode == selection.focusNode.parentNode.parentNode) {
        if ((selection.anchorOffset == selection.focusOffset) && (selection.anchorNode.parentNode == selection.focusNode.parentNode)) {
        } else {

            var anchor = 0;
            var i = 0;
            while (obj.contentElt.childNodes[i].firstChild !== selection.anchorNode) {
                anchor += obj.contentElt.childNodes[i].innerText.length;
                i++;
            }
            anchor += selection.anchorOffset;

            var focus = 0;
            var i = 0;
            while (obj.contentElt.childNodes[i].firstChild !== selection.focusNode) {
                focus += obj.contentElt.childNodes[i].innerText.length;
                i++;
            }
            focus += selection.focusOffset;

            var newObj = {};
            if (anchor > focus) {
                newObj.start = focus;
                newObj.stop = anchor;
            } else {
                newObj.start = anchor;
                newObj.stop = focus;
            }
            newObj.children = [];

            // create DOM
            div = document.createElement('div');
            div.id = "write";
            div.classList.add('obj')
            div.classList.add('open')
            //div.style.left = window.pageXOffset + e.clientX + 0 + 'px';
            //div.style.top = window.pageYOffset + e.clientY + 0 + 'px';
            div.style.left = clientX + 'px';
            div.style.top = clientY + 'px';
            document.body.appendChild(div);
            
            var header = document.createElement('div');
            header.classList.add('header');
            header.innerText = 'new note';
            div.appendChild(header);

            var textarea = document.createElement('textarea');
            textarea.classList.add('content');
            div.appendChild(textarea);
            textarea.focus();

            var input = document.createElement('input');
            input.type = 'button';
            input.value = 'cancel';
            input.addEventListener('click', cancel, false);
            input.style.marginRight = '10px';
            div.appendChild(input);

            var input = document.createElement('input');
            input.type = 'button';
            input.value = 'post';
            input.addEventListener('click', function() {
                if (textarea.value.length > 0) {
                    newObj.content = textarea.value;
                    newObj.path = [...obj.path];
                    newObj.username = document.getElementById("username").value;
                    post("post.php", JSON.parse(JSON.stringify(newObj)), obj);
                }
                cancel();
            });
            div.appendChild(input);
            
            writeElt = div;

            // draggable
            $(function() {
                $('#write').draggable({ handle: '.header', containment: $('#container') });
            });

        }
    }

}



function cancel() {
    if (document.body.contains(writeElt)) writeElt.parentNode.removeChild(writeElt); 
}



// OBJ

function toggleChildren(e) {

    var obj = this;
    console.log(obj);
    console.log(obj.objElt.getBoundingClientRect());
    console.log(left = window.pageXOffset || document.documentElement.scrollLeft);

    children = e.target.dataset.childrenIds.split(' ').sort();
    for (var i = 0; i < children.length; i++) {
        obj.children[children[i]].objElt.classList.add("open");
        obj.children[children[i]].objElt.style.top = window.pageYOffset + e.clientY + 0 + i*2 + 'px';
        obj.children[children[i]].objElt.style.left = window.pageXOffset + obj.objElt.getBoundingClientRect().x + width + 48 + i*2 + 'px';
        //obj.objElt.style.left = (obj.path.length) * width + (obj.path.length) * unit * 3 + unit * 1 + "px";
    }

}


function createHighlights(obj) {

    // create a sorted clone of obj.children array
    var children = [];
    for (var i in obj.children) {
        var child = {};
        child.id = obj.children[i].id;
        child.start = obj.children[i].start;
        child.stop = obj.children[i].stop;
        children.push(child);
    }
    sorted = children.sort(function(a, b) {
        return parseFloat(a.start) - parseFloat(b.start);
    });

    // divide obj.content into nodes using children positions 
    var nodes = [];
    var index = 0;
    while (index < obj.content.length) {
        var node = {};
        node.start = index;
        node.stop = obj.content.length;
        node.children = [];
        for (var i in sorted) {
            if ((sorted[i].start > index) && (sorted[i].start < node.stop)) node.stop = sorted[i].start; 
            if (sorted[i].start == index) node.children.push(sorted[i].id); 
            if ((sorted[i].stop > index) && (sorted[i].stop < node.stop)) {
                node.stop = sorted[i].stop;
                if (node.children.indexOf(sorted[i].id) === -1) node.children.push(sorted[i].id);
            }
            if ((sorted[i].start < index) && (sorted[i].stop > index)) {
                if (node.children.indexOf(sorted[i].id) === -1) node.children.push(sorted[i].id); 
            }
        }
        nodes.push(node);
        index = node.stop;
    }

    // create dom
    obj.nodes = nodes;
    while (obj.contentElt.firstChild) obj.contentElt.removeChild(obj.contentElt.firstChild);
    for (var i = 0; i < obj.nodes.length; i++) {
        var span = document.createElement('span');
        span.innerText = obj.content.substring(obj.nodes[i].start, obj.nodes[i].stop);
        if (obj.nodes[i].children.length > 0) {
            span.dataset.childrenIds = obj.nodes[i].children.join(' '); 
            span.dataset.childrenLength = obj.nodes[i].children.length;
            span.addEventListener('click', toggleChildren.bind(obj), false);
        }
        obj.contentElt.appendChild(span);
    }

}



function createObj(obj, par) {

    // create DOM elements 
    obj.objElt = document.createElement('div')
    obj.objElt.classList.add('obj');
    containerElt.appendChild(obj.objElt);
    
    obj.headerElt = document.createElement('div')
    obj.headerElt.classList.add('header');
    obj.headerElt.innerText = obj.datetime + ' - ' + obj.username;
    obj.objElt.appendChild(obj.headerElt);

    obj.contentElt = document.createElement('div')
    obj.contentElt.classList.add('content');
    obj.objElt.appendChild(obj.contentElt);
    obj.contentElt.addEventListener("mouseup", createAddElt.bind(obj), false);
    
    var span = document.createElement('span');
    span.innerText = obj.content;
    obj.contentElt.appendChild(span);

    // draggable
    $(function() {
        $('.obj').draggable({ handle: '.header', containment: $('#container') });
    });

    // give id to children and createHighlights
    for (var i = 0; i < obj.children.length; i++) obj.children[i].id = i;
    if (obj.children.length > 0) createHighlights(obj);

    // build path and define display position
    obj.path = [];
    
    if (par) {
        obj.par = par;
        obj.path = [...obj.par.path];
        obj.path.push(obj.id);
        //obj.objElt.style.left = (obj.path.length) * width + (obj.path.length) * unit * 3 + unit * 1 + "px";
    } else {
        root = obj;
        obj.objElt.id = "root";
        obj.objElt.style.left = unit + "px";
        obj.objElt.style.top = unit + "px";
    }

    // call function for children
    for (var i in obj.children) createObj(obj.children[i], obj);

}



// POST 

function post(url, obj, par) {
    console.log(obj);
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(obj)
    })
    .then(function(response) {
        return response.json();
    })
    .then(function(json) {
        console.log(json);
        if (json.status === "sucess") {
            obj.id = par.children.length;
            obj.datetime = json.datetime;
            obj.username = json.username;
            par.children.push(obj)
            createObj(obj, par); 
            obj.objElt.classList.add("open");
            obj.objElt.style.top = clientY + 'px';
            obj.objElt.style.left = window.pageXOffset + par.objElt.getBoundingClientRect().x + width + 48 + 'px';
            createHighlights(par);
        }
    });
}



// INIT

var unit = 16;
var width = 320;
var clientX;
var clientY;
var containerElt = document.getElementById('container');;
var rootElt;
var writeElt;
var addElt;

function init(file) {

    fetch(file, {cache: 'no-store', })
        .then(function(response) {
            return response.json();
        })
        .then(function(json) {
            if (Object.keys(json).length > 0) {
                createObj(json)
                console.log(root);
            } else {
                createWriteElt()
            }
        });

}

init('data.json');

//document.addEventListener("mousedown", function() {
//    if (document.body.contains(addElt)) addElt.parentNode.removeChild(addElt); 
//}, false);


// INTERFACE

var zoom = 100;

function zoomin () {
    console.log('zoomin', zoom);
    if (zoom < 100) {
        zoom += 10;
    }
}

function zoomreset() {
    console.log('zoomreset', zoom);
        zoom = 100;
        containerElt.style.transform = 'scale('+1+')';
}

function zoomout () {
    console.log('zoomout', zoom);
    if (zoom > 10) {
        zoom -= 10;
        containerElt.style.transform = 'scale('+zoom/100+')';
    }
}

document.getElementById('zoomin').addEventListener('click', zoomin, false);
document.getElementById('zoomreset').addEventListener('click', zoomreset, false);
document.getElementById('zoomout').addEventListener('click', zoomout, false);
