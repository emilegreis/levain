<?php

    function setDeep(&$array, $path, $value) { 
        $tempArr = &$array; 
        foreach($path as $key) { 
            $tempArr = &$tempArr['children'][$key]; 
        } 
        array_push($tempArr['children'], $value);
    }

    $data = json_decode(file_get_contents("data.json"), true);
    $POST = json_decode(file_get_contents('php://input'), true);
    
    $POST['datetime'] = date("Y/m/d H:i:s"); 
    $POST['username'] = 'username'; 

    $path = $POST['path'];
    unset($POST['path']);
    #print($path);

    setDeep($data, $path, $POST);

    $data = json_encode($data, JSON_PRETTY_PRINT);
    if (file_put_contents('data.json', $data)) {
        $POST['status'] = 'sucess';
        echo json_encode($POST);
    } else {
        $POST['status'] = 'error';
        echo json_encode($POST);
    }

